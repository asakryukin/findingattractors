package newAlgorithm;

import java.util.ArrayList;
import java.util.List;

import mainPackage.BitString;
import mainPackage.FVSComparator;

public class NewGreedyAlgorithmBoolean extends NewAlgBoolean{
	private boolean stop=false;
	public NewGreedyAlgorithmBoolean(String filename) {
		super(filename);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	protected List<String> findStableStates() {
		// TODO Auto-generated method stub
		List<BitString> result=new ArrayList<BitString>();
		List<Integer> nodesIn=new ArrayList<Integer>();
		List<Integer> nodesNotIn=new ArrayList<Integer>();
		for(int i=0;i<numberOfNodes;i++){
			nodesNotIn.add(i);
		}
		for(int i=0;i<numberOfNodes-1;i++){
			int best=0;
			int node_number=-1;
			List<BitString> tentative_result=new ArrayList<BitString>(); 
			List<List<BitString>> candidateResults=new ArrayList<List<BitString>>(); 
			List<Integer> candidates=new ArrayList<Integer>();
			for (int k=0;k<nodesNotIn.size();k++){
				//start checking different combinations
				int attr_length=0;
				nodesIn.add(nodesNotIn.get(k));
				nodesIn.sort(new FVSComparator());
				System.out.println();
				System.out.print("Adding node number:"+(i+1));
				List<BitString> temp=new ArrayList<BitString>();
				
				List<List<BitString>> t=updateEntry(result, nodesIn.indexOf(nodesNotIn.get(k)));
					temp.addAll(t.get(0));
					temp.addAll(t.get(1));
						//counting DFS size, by eliminating
						for(int j=0;j<temp.size();j++){
							int curr=usedNodes;
							if(!eliminateRepresentatives(temp.get(j), new ArrayList<BitString>(), temp, nodesIn)){
								temp.remove(j);
								j--;
								
								
							}else{
								attr_length+=usedNodes-curr+1;
								if (attr_length>best && best!=0){
									stop=true;
									break;
								}
							}
						}
						if(stop){
							//if already longer remove it
							nodesIn.remove(nodesIn.indexOf(nodesNotIn.get(k)));
							stop=false;
							continue;
						}
						if(best==0||best>attr_length){
							//if it is better
							candidates=new ArrayList<Integer>();
							candidateResults=new ArrayList<List<BitString>>();
							best=attr_length;
							node_number=nodesNotIn.get(k);
							tentative_result=new ArrayList<BitString>();
							for(int ind=0;ind<temp.size();ind++){
								tentative_result.add(temp.get(ind));
							}
							candidates.add(node_number);
							candidateResults.add(tentative_result);
						}else if(best==attr_length){
							node_number=nodesNotIn.get(k);
							tentative_result=new ArrayList<BitString>();
							for(int ind=0;ind<temp.size();ind++){
								tentative_result.add(temp.get(ind));
							}
							candidates.add(node_number);
							candidateResults.add(tentative_result);
						}
						
						nodesIn.remove(nodesIn.indexOf(nodesNotIn.get(k)));
						
						System.out.println(" Number of Representative States:"+result.size());
			}
			//add best
			int selected=(int) Math.floor(candidates.size()*Math.random());
			nodesIn.add(candidates.get(selected));
			nodesIn.sort(new FVSComparator());
			nodesNotIn.remove(nodesNotIn.indexOf(candidates.get(selected)));
			result=new ArrayList<BitString>();
			for(int ind=0;ind<candidateResults.get(selected).size();ind++){
				result.add(candidateResults.get(selected).get(ind));
			}
			
		
		
		}
		
		nodesIn.add(nodesNotIn.get(0));
		nodesIn.sort(new FVSComparator());
		
		System.out.println();
		//System.out.print("Adding node number:"+(nodesNotIn.get(0)+1));
		
		List<BitString> temp=new ArrayList<BitString>();
	
		List<List<BitString>> t=updateEntry(result, nodesIn.indexOf(nodesNotIn.get(0)));
		temp.addAll(t.get(0));
		temp.addAll(t.get(1));
		result=temp;
		
		List<String> stringResult=new ArrayList<String>();
		
		for(int i=0;i<result.size();i++){
			String tt="";
			for(int j=0;j<result.get(i).size();j++){
				if(result.get(i).get(j)){
					tt+="1";
				}else{
					tt+="0";
				}
			}
			stringResult.add(tt);
		}
		
		return stringResult;
	}
	/*
	@Override
	protected boolean eliminateRepresentatives(String candidate,
			List<String> in, List<String> list, List<Integer> nodesIn) {
		boolean result=true;
		in.add(candidate);
		usedNodes++;
		List<String> nei=findNeighbours(candidate,nodesIn);
		for(int i=0;i<nei.size();i++){
			if(!in.contains(nei.get(i))){
				if(list.contains(nei.get(i))){
					usedNodes++;
					return false;
				}else{
					if (eliminateRepresentatives(nei.get(i), in, list,nodesIn)){
						
					}else{
						result=false;
					}
					//return eliminateRepresentatives(nei.get(i), in, list,nodesIn);
				}
			}
		}
		
		return result;
	}
	*/
}
