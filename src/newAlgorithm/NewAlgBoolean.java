package newAlgorithm;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import mainPackage.AttractorComparator;
import mainPackage.AttractorElementComparator;
import mainPackage.BitString;
import mainPackage.FVSComparator;

public class NewAlgBoolean {
	protected int numberOfNodes;
	protected List<List<Integer>> listOfNeighbours;
	protected List<List<Boolean>> truthTable;
	protected List<List<String>> attractors=new ArrayList<List<String>>();
	protected List<String> stableStates=new ArrayList<String>();
	protected List<String> visitedNodes=new ArrayList<String>();
	protected int usedNodes=0;
	protected boolean is_ordered=true;
	public NewAlgBoolean(String filename) {
			
		if(parseData(filename)){
			attractors=findAttractor();
			attractors.sort(new AttractorComparator());
			for(int i=0;i<attractors.size();i++){
				attractors.get(i).sort(new AttractorElementComparator());
			}
		}else{
			System.out.println("Error reading the file");
		}
		
		
	}
	public NewAlgBoolean(String filename,boolean io) {
		is_ordered=io;
		if(parseData(filename)){
			attractors=findAttractor();
			attractors.sort(new AttractorComparator());
			for(int i=0;i<attractors.size();i++){
				attractors.get(i).sort(new AttractorElementComparator());
			}
		}else{
			System.out.println("Error reading the file");
		}
		
		
	}
	
	public int numberOfVisitedNodes(){
		return usedNodes;
	}
	public List<List<String>> getAttractors(){
		return attractors;
	}
	protected boolean parseData(String filename){
		// TODO Auto-generated constructor stub
				Path path=Paths.get(filename);
				BufferedReader reader;
				try {
					reader = Files.newBufferedReader(path);
					
					numberOfNodes=Integer.parseInt(reader.readLine());
					listOfNeighbours = new ArrayList<List<Integer>>(numberOfNodes);
					truthTable=new ArrayList<List<Boolean>>(numberOfNodes);
					String line;
					int i=0;
					while (i<numberOfNodes){
						List<Integer> l=new ArrayList<Integer>();
						if((line=reader.readLine())!=null && line.length()>2){
							line+=" ";
						line=line.replaceAll("\\s+", " ");
						int s=line.indexOf(" ");;
						while((s+1)<line.length()){
							
							int s1=line.substring(s+1).indexOf(" ");
							s1=s1+s+1;
							l.add(Integer.parseInt(line.substring(s+1, s1)));
							s=s1;
							
						}
						i++;
						listOfNeighbours.add(l);
					}
					
					}
					i=0;
					while (i<numberOfNodes){
						List<Boolean> l=new ArrayList<Boolean>();
					if((line=reader.readLine())!=null && line.length()>2){
						line+=" ";
						line=line.replaceAll("\\s+", " ");
						int s=-1;
						while((s+1)<line.length()){
							
							int s1=line.substring(s+1).indexOf(" ");
							s1=s1+s+1;
							l.add((Integer.parseInt(line.substring(s+1, s1))==1));
							s=s1;
							
						}
						i++;
						truthTable.add(l);
					}
					
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					
					e.printStackTrace();
					return false;
				}
				try {
					
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;
	}
	
	protected List<List<String>> findAttractor(){
		
		stableStates=findStableStates();
		
		attractors=new ArrayList<List<String>>();
		List<String> fp=new ArrayList<String>();
		
		for(int i=0;i<stableStates.size();i++){
			fp.add(stableStates.get(i));
		}
		
		while (fp.size()>0){
			List<String> visited=new ArrayList<String>();
			List<String> res=new ArrayList<String>();
			visited.add(fp.get(0));
			fp.remove(0);
			
			res=list_dfs(visited.get(0),visited,fp,listOfNeighbours,truthTable);
			if (res!=null){
			if(res.size()>0){
				usedNodes+=res.size();
				attractors.add(res);
			}
			}
			/*
			for(int i=0;i<visited.size();i++){
				if(!visitedNodes.contains(visited.get(i))){
					visitedNodes.add(visited.get(i));
				}
			}*/
			
		}
		
		return attractors;
	}
	private List<String> list_dfs(String current,List<String> visited,List<String> fp,List<List<Integer>> list,List<List<Boolean>> truth){
		List<String> attractor=new ArrayList<String>();
		attractor.add(current);
		int[] var=new int[numberOfNodes];
		
		//get byte representation of the string
		for(int i=0;i<numberOfNodes;i++){
			var[i]=Character.getNumericValue(current.charAt(i));
		}
		
		
		for(int i=0;i<numberOfNodes;i++){
			//going through neighbours of every node
			List<Integer> nei=list.get(i);
			int sum=0;
			//sum is a current state like 100 = 4
			for(int j=0;j<nei.size();j++){
				sum=sum+var[nei.get(j)-1]*((int) Math.pow(2, nei.size()-j-1));
			}
			
			//next state
			int num=0;
			if(truth.get(i).get(sum)){
				num=1;
			}
			
			//if next state is different from the present do
			if(num!=var[i]){
				String r="";
				//constructing next state
				for(int j=0;j<numberOfNodes;j++){
					if(j!=i){
						r=r+var[j];
					}else{
						r=r+num;
					}
				}
				
				//checking whether it was visited before
				boolean is_ok=true;
				for(int j=0;j<visited.size();j++){
					if(visited.get(j).equalsIgnoreCase(r)){
						is_ok=false;
						break;
					}
				}
				//if not continue dfs
				if(is_ok){
					//checking whether this state is in the fixed points
					for(int j=0;j<stableStates.size();j++){
						if((stableStates.get(j).equalsIgnoreCase(r))&&(!r.equalsIgnoreCase(current))){
							stableStates.remove(visited.get(0));
							usedNodes+=visited.size();
							return null;
						}
					}
					visited.add(r);
					List<String> nn=list_dfs(r, visited, fp, list, truth);
					if(nn!=null){
					attractor.addAll(nn);}
					else{
						return null;
					}
				}
				
				
				
			}
			
		}
		
	return attractor;
	}
	protected List<String> findStableStates(){
		
		List<BitString> result=new ArrayList<BitString>();
		List<Integer> nodesIn=new ArrayList<Integer>();
		
		
		for(int i=0;i<numberOfNodes;i++){
			int XXX;
			//nodesIn.add(i);
			//XXX=i;
			if(!is_ordered){
				 XXX=determineNewNode(nodesIn);
			}
			else{
			do
			{
				XXX=(int) Math.floor(numberOfNodes*Math.random());
			}
			while(nodesIn.contains(XXX));
			}
			//XXX=i;
			nodesIn.add(XXX);
			nodesIn.sort(new FVSComparator());
			
			System.out.println();
			System.out.print("Adding node number:"+(i+1));
			
		List<BitString> temp=new ArrayList<BitString>();
		
		List<List<BitString>> t=updateEntry(result, nodesIn.indexOf(XXX));
		
			temp.addAll(t.get(0));
			temp.addAll(t.get(1));
		
		result=temp;
		temp=null;
		t=null;
		if(i<numberOfNodes-1){
			
		for(int j=0;j<result.size();j++){
			
			if(!eliminateRepresentatives(result.get(j), new ArrayList<BitString>(), result, nodesIn)){
				result.remove(j);
				j--;
			}
		}
		
		}
		System.out.println(" Number of Representative States:"+result.size());
		}
		
		List<String> stringResult=new ArrayList<String>();
		
		for(int i=0;i<result.size();i++){
			String t="";
			for(int j=0;j<result.get(i).size();j++){
				if(result.get(i).get(j)){
					t+="1";
				}else{
					t+="0";
				}
			}
			stringResult.add(t);
		}
		
		return stringResult;
		
	}
	
	protected int determineNewNode(List<Integer> in){
		List<Integer> nN=new ArrayList<Integer>();
		
		for(int i=0;i<numberOfNodes;i++){
			int notPresent=0;
			if(!in.contains(i)){
				List<Integer> nei=listOfNeighbours.get(i);
				for(int j=0;j<nei.size();j++){
					if(!in.contains(nei.get(j)-1)){
						notPresent++;
					}
				}
				nN.add(notPresent);
			}else{
				nN.add(numberOfNodes+1);
			}
			
		}
		int iMin,min;
		iMin=0;
		min=nN.get(0);
		List<Integer> candidates=new ArrayList<Integer>();
		candidates.add(iMin);
		for(int i=1;i<nN.size();i++){
			if(nN.get(i)<min){
				min=nN.get(i);
				iMin=i;
				candidates=new ArrayList<Integer>();
				candidates.add(iMin);
			}else if(nN.get(i)==min){
				candidates.add(i);
			}
		}
		iMin=candidates.get((int) Math.floor(candidates.size()*Math.random()));
		return iMin;
	}
	public boolean containsBitString(List<BitString> list,BitString b){
		
		for(int i=0;i<list.size();i++){
			if(list.get(i).equalsBS(b)){
				
				return true;
			}
		}
		
		return false;
	}
	protected boolean eliminateRepresentatives(BitString candidate,List<BitString> in,List<BitString> list,List<Integer> nodesIn){
		boolean result=true;
		in.add(candidate);
		usedNodes++;
		
		List<BitString> nei=findNeighbours(candidate,nodesIn);
		//long startTime = System.currentTimeMillis();
		for(int i=0;i<nei.size();i++){
			
			if(!containsBitString(in,nei.get(i))){
				if(containsBitString(list,nei.get(i))){
					usedNodes++;
					return false;
				}else{
					if (eliminateRepresentatives(nei.get(i), in, list,nodesIn)){
						
					}else{
						//result=false;
						return false;
					}
					//return eliminateRepresentatives(nei.get(i), in, list,nodesIn);
				}
			}
			
		}
		//long stopTime = System.currentTimeMillis();
		//System.out.println("Execution time: "+(stopTime-startTime)+"ms");
		return result;
	}
	
	protected List<BitString> findNeighbours(BitString candidate,List<Integer> nodesIn){
		//IMPORTANT: nodes are added in order
		
		List<BitString> result=new ArrayList<BitString>();
		for(int ind=0;ind<candidate.size();ind++){
			List<Integer> neighbours=listOfNeighbours.get(nodesIn.get(ind));
			List<Integer> notIn=new ArrayList<Integer>();
			notIn=whetherAllIn(nodesIn, neighbours);
			if(notIn.isEmpty()){
				
					
					BitString var=new BitString(candidate.size());
					var.or(candidate);
					int sum=0;
					boolean newVal=false;
					for(int j=0;j<neighbours.size();j++){
						if(candidate.get(nodesIn.indexOf(neighbours.get(j)-1)))
						sum+=Math.pow(2, neighbours.size()-1-j);
					}
					if(truthTable.get(nodesIn.get(ind)).get(sum))
						newVal=true;
					if(candidate.get(nodesIn.indexOf(nodesIn.get(ind)))!=newVal){
						var.flip(nodesIn.indexOf(nodesIn.get(ind)));
						
						if(!containsBitString(result, var)){
							result.add(var);
						}
					}
				
			}else{
				//notIn.sort(new SimpleIntComparator());
				BitString var=new BitString(candidate.size());
				var.or(candidate);
					boolean changing=true;
					int[] nVal=new int[neighbours.size()];
					
					for(int j=0;j<Math.pow(2, notIn.size());j++){
						int[] temp=new int[notIn.size()];
						int val=j;
						for(int k=0;k<notIn.size();k++){
							temp[notIn.size()-k-1]=val%2;
							val/=2;
						}
						
					for(int k=0;k<neighbours.size();k++){
						if(notIn.contains(neighbours.get(k))){
							nVal[k]=temp[notIn.indexOf(neighbours.get(k))];
						}else{
							if(candidate.get(nodesIn.indexOf(neighbours.get(k)-1))){
								nVal[k]=1;
							}else{
								nVal[k]=0;
							}
						}
					}
					int sum=0;
					boolean newVal=false;
					for(int k=0;k<neighbours.size();k++){
						sum+=nVal[k]*Math.pow(2, neighbours.size()-1-k);
					}
					if(truthTable.get(nodesIn.get(ind)).get(sum))
						newVal=true;
					if(var.get(nodesIn.indexOf(nodesIn.get(ind)))==newVal){
						changing=false;
						break;
						}
					}
					if(changing){
						boolean newVal=false;
						if(newVal==var.get(nodesIn.indexOf(nodesIn.get(ind))))
							newVal=true;
						var.set(nodesIn.indexOf(nodesIn.get(ind)),newVal);
						
						if(!containsBitString(result, var)){
							result.add(var);
						}
					}
					
				}
		}
		return result;
	}
	
	protected List<String> checkStates(List<List<String>> graph,String current,List<String> in)
	{
		for(int i=0;i<graph.size();i++){
			if(graph.get(i).get(0).equals(current)){
				for(int j=1;j<graph.get(i).size();j++){
					if(!in.contains(graph.get(i).get(j))){
						in.add(graph.get(i).get(j));
						checkStates(graph, graph.get(i).get(j), in);
					}
				}
			}
		}
		return in;
	}
	
	protected List<List<BitString>> updateEntry(List<BitString> line,int position){
		List<List<BitString>> res=new ArrayList<List<BitString>>();
		List<BitString> temp=new ArrayList<BitString>();
		if (line.size()==0){
			BitString t=new BitString(1);
			t.set(0,false);
			temp.add(t);
			res.add(temp);
			temp=new ArrayList<BitString>();
			BitString t1=new BitString(1);
			t1.set(0,true);
			temp.add(t1);
			res.add(temp);
		}else{
			
		for(int i=0;i<line.size();i++){
			int length=line.get(i).size();
			BitString t=new BitString(length+1);
			for(int j=0;j<position;j++){
				t.set(j, line.get(i).get(j));
			}
			t.set(position,false);
			for(int j=position+1;j<length+1;j++){
				t.set(j, line.get(i).get(j-1));
			}
			temp.add(t);
		}
		
		res.add(temp);
		temp=null;
		temp=new ArrayList<BitString>();
		for(int i=0;i<line.size();i++){
			int length=line.get(i).size();
			BitString t=new BitString(length+1);
			for(int j=0;j<position;j++){
				t.set(j, line.get(i).get(j));
			}
			t.set(position,true);
			for(int j=position+1;j<length+1;j++){
				t.set(j, line.get(i).get(j-1));
			}
			temp.add(t);
		}
		res.add(temp);
		}
		return res;
	}
	
	
	protected List<Integer> whetherAllIn(List<Integer> nodesIn,List<Integer> neighbours){
		List<Integer> res=new ArrayList<Integer>();
		for(int i=0;i<neighbours.size();i++){
			if(!nodesIn.contains(neighbours.get(i)-1)){
				res.add(neighbours.get(i));
			}
		}
		return res;
	}
	
}
