package newAlgorithm;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import mainPackage.BitString;
import mainPackage.FVSComparator;

public class FindFixedPointsByMerging extends NewAlgBoolean{
	private List<String> FP;
	private boolean stop=false;
	private PrintWriter writer;
	public FindFixedPointsByMerging(String filename) {
		super(filename);
		try {
			writer=new PrintWriter("steps"+".txt", "UTF-8");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated constructor stub
		FP=findStableStates();
	}
	
	public List<String>  getFixedPoints() {
		// TODO Auto-generated method stub
		return FP;
	}
	
	@Override
	protected List<List<String>> findAttractor() {
		// TODO Auto-generated method stub
		List<List<String>> result=new ArrayList<List<String>>();
		return result;
	}
	
	@Override
	protected List<String> findStableStates() {
		// TODO Auto-generated method stub

		List<BitString> result=new ArrayList<BitString>();
		List<Integer> nodesIn=new ArrayList<Integer>();
		List<Integer> nodesNotIn=new ArrayList<Integer>();
		
		//Firstly create an array with all nodes and candidate state for each
		List<List<Integer>> clusters=new ArrayList<List<Integer>>();
		List<List<BitString>> states=new ArrayList<List<BitString>>();
		
		for(int i=0;i<numberOfNodes;i++){
			List<Integer> temp=new ArrayList<Integer>();
			temp.add(i);
			List<BitString> tstates=new ArrayList<BitString>();
			List<List<BitString>> t=updateEntry(new ArrayList<BitString>(), 0);
			tstates.addAll(t.get(0));
			tstates.addAll(t.get(1));
				//counting DFS size, by eliminating
				for(int j=0;j<tstates.size();j++){
					//int curr=usedNodes;
					if(!ifN(tstates.get(j), temp)){
						tstates.remove(j);
						j--;
						
						
					}
				}
			clusters.add(temp);
			states.add(tstates);
		}
		
		for(int i=0;i<clusters.size();i++){
			writer.print("(");
			for(int j=0;j<clusters.get(i).size();j++){
				writer.print(""+(clusters.get(i).get(j)+1)+" ");
			}
			writer.print(")[");
			writer.print(states.get(i).size());
			writer.print("]\t");
		}
		writer.println();
		writer.flush();
		
		
		while(clusters.size()>1){
			
			shuffle(clusters,states);
			
			
			merge(clusters,states);
			
			for(int i=0;i<clusters.size();i++){
				writer.print("(");
				for(int j=0;j<clusters.get(i).size();j++){
					writer.print(""+(clusters.get(i).get(j)+1)+" ");
				}
				writer.print(")[");
				writer.print(states.get(i).size());
				writer.print("]\t");
			}
			writer.println();
			writer.flush();
		}
		writer.close();
		List<String> stringResult=new ArrayList<String>();
		
		for(int i=0;i<states.get(0).size();i++){
			String tt="";
			for(int j=0;j<states.get(0).get(i).size();j++){
				if(states.get(0).get(i).get(j)){
					tt+="1";
				}else{
					tt+="0";
				}
			}
			stringResult.add(tt);
		}
		return stringResult;
	}
	
	private int fNE(List<List<BitString>> clusters){
		int result=0;
		for(int i=0;i<clusters.size();i++){
			result+=clusters.get(i).size();
		}
		
		return result;
	}
	
	private void shuffle(List<List<Integer>> clusters,List<List<BitString>> states){
		
		for(int i=0;i<clusters.size();i++){
			List<Integer> t1=new ArrayList<Integer>();
			List<BitString> t2=new ArrayList<BitString>();
			int r=(int) Math.round(Math.random()*(clusters.size()-1));
			
			t1=clusters.get(i);
			t2=states.get(i);
			
			clusters.set(i, clusters.get(r));
			states.set(i, states.get(r));
			
			clusters.set(r, t1);
			states.set(r, t2);
			
		}
		
	}
	
	private void merge(List<List<Integer>> clusters,List<List<BitString>> states){
		int i=1;
		
		int indi=0,indj=1,max=0;
		List<Integer> combined=new ArrayList<Integer>();
		combined.addAll(clusters.get(indi));
		combined.addAll(clusters.get(indj));
		combined.sort(new SimpleIntComparator());
		max=combine(clusters.get(indi),clusters.get(indj),states.get(indi),states.get(indj),combined).size();
		
		
		
		for(i=0;i<clusters.size()-1;i=i+2){
			int tempM=0;
			
			for(int j=i+2;j<clusters.size();j++){
				combined=new ArrayList<Integer>();
				combined.addAll(clusters.get(i));
				combined.addAll(clusters.get(j));
				combined.sort(new SimpleIntComparator());
				tempM=combine(clusters.get(i),clusters.get(j),states.get(i),states.get(j),combined).size();
				if(tempM<max){
					max=tempM;
					indi=i;
					indj=j;
				}
			}
			
			/*
			List<Integer> temp=new ArrayList<Integer>();
			
			temp=clusters.get(i+1);
			clusters.set(i+1, clusters.get(ind));
			clusters.set(ind,temp);
			
			List<BitString> tempS=new ArrayList<BitString>();
			
			tempS=states.get(i+1);
			states.set(i+1, states.get(ind));
			states.set(ind,tempS);
			*/
		}
		
		combined=new ArrayList<Integer>();
		combined.addAll(clusters.get(indi));
		combined.addAll(clusters.get(indj));
		combined.sort(new SimpleIntComparator());
		states.set(indi,combine(clusters.get(indi),clusters.get(indj),states.get(indi),states.get(indj),combined));
		clusters.set(indi, combined);
		states.remove(indj);
		clusters.remove(indj);
		System.out.print("Number of clsuters:"+clusters.size());
		System.out.println("   Number of elements:"+fNE(states));
		
		/*
		
		for(i=1;i<clusters.size();i++){
			List<Integer> combined=new ArrayList<Integer>();
			combined.addAll(clusters.get(i));
			combined.addAll(clusters.get(i-1));
			combined.sort(new SimpleIntComparator());
			states.set(i-1,combine(clusters.get(i-1),clusters.get(i),states.get(i-1),states.get(i),combined));
			clusters.set(i-1, combined);
			states.remove(i);
			clusters.remove(i);
			System.out.print("Number of clsuters:"+clusters.size());
			System.out.println("   Number of elements:"+fNE(states));
			
		}
		/*
		i=i-2;
		for(i=i;i>0;i=i-2){
			
		}
		*/
	}
	
	private List<BitString> combine(List<Integer> c1,List<Integer> c2,List<BitString> s1,List<BitString> s2,List<Integer> combined)
	{
		List<BitString> result=new ArrayList<BitString>();
		
		for(int i=0;i<s1.size();i++){
			
			for(int j=0;j<s2.size();j++){
				BitString t=new BitString(s1.get(0).length+s2.get(0).size());
				for(int k=0;k<s1.get(i).length;k++){
					t.set(combined.indexOf(c1.get(k)), s1.get(i).get(k));
				}
				for(int k=0;k<s2.get(j).length;k++){
					t.set(combined.indexOf(c2.get(k)), s2.get(j).get(k));
				}
				if(ifN(t,combined))
					result.add(t);
			}
			
		}
		
		return result;
		
	}
	protected boolean ifN(BitString candidate,List<Integer> nodesIn){
		//IMPORTANT: nodes are added in order
		
		List<BitString> result=new ArrayList<BitString>();
		for(int ind=0;ind<candidate.size();ind++){
			List<Integer> neighbours=listOfNeighbours.get(nodesIn.get(ind));
			List<Integer> notIn=new ArrayList<Integer>();
			notIn=whetherAllIn(nodesIn, neighbours);
			if(notIn.isEmpty()){
				
					
					//BitString var=new BitString(candidate.size());
					//var.or(candidate);
					int sum=0;
					boolean newVal=false;
					for(int j=0;j<neighbours.size();j++){
						if(candidate.get(nodesIn.indexOf(neighbours.get(j)-1)))
						sum+=Math.pow(2, neighbours.size()-1-j);
					}
					if(truthTable.get(nodesIn.get(ind)).get(sum))
						newVal=true;
					if(candidate.get(nodesIn.indexOf(nodesIn.get(ind)))!=newVal){
						return false;
						/*var.flip(nodesIn.indexOf(nodesIn.get(ind)));
						
						if(!containsBitString(result, var)){
							result.add(var);
							return false;
						}*/
					}
				
			}else{
				//notIn.sort(new SimpleIntComparator());
				//BitString var=new BitString(candidate.size());
				//var.or(candidate);
					boolean changing=true;
					int[] nVal=new int[neighbours.size()];
					
					for(int j=0;j<Math.pow(2, notIn.size());j++){
						int[] temp=new int[notIn.size()];
						int val=j;
						for(int k=0;k<notIn.size();k++){
							temp[notIn.size()-k-1]=val%2;
							val/=2;
						}
						
					for(int k=0;k<neighbours.size();k++){
						if(notIn.contains(neighbours.get(k))){
							nVal[k]=temp[notIn.indexOf(neighbours.get(k))];
						}else{
							if(candidate.get(nodesIn.indexOf(neighbours.get(k)-1))){
								nVal[k]=1;
							}else{
								nVal[k]=0;
							}
						}
					}
					int sum=0;
					boolean newVal=false;
					for(int k=0;k<neighbours.size();k++){
						sum+=nVal[k]*Math.pow(2, neighbours.size()-1-k);
					}
					if(truthTable.get(nodesIn.get(ind)).get(sum))
						newVal=true;
					if(candidate.get(nodesIn.indexOf(nodesIn.get(ind)))==newVal){
						changing=false;
						break;
						}
					}
					if(changing){
						return false;
						/*
						boolean newVal=false;
						if(newVal==var.get(nodesIn.indexOf(nodesIn.get(ind))))
							newVal=true;
						var.set(nodesIn.indexOf(nodesIn.get(ind)),newVal);
						
						if(!containsBitString(result, var)){
							result.add(var);
							return false;
						}*/
					}
					
				}
		}
		return true;
	}
	
	protected List<List<BitString>> updateEntry(List<BitString> line,int position){
		List<List<BitString>> res=new ArrayList<List<BitString>>();
		List<BitString> temp=new ArrayList<BitString>();
		if (line.size()==0){
			BitString t=new BitString(1);
			t.set(0,false);
			temp.add(t);
			res.add(temp);
			temp=new ArrayList<BitString>();
			BitString t1=new BitString(1);
			t1.set(0,true);
			temp.add(t1);
			res.add(temp);
		}else{
			
		for(int i=0;i<line.size();i++){
			int length=line.get(i).size();
			BitString t=new BitString(length+1);
			for(int j=0;j<position;j++){
				t.set(j, line.get(i).get(j));
			}
			t.set(position,false);
			for(int j=position+1;j<length+1;j++){
				t.set(j, line.get(i).get(j-1));
			}
			temp.add(t);
		}
		
		res.add(temp);
		temp=null;
		temp=new ArrayList<BitString>();
		for(int i=0;i<line.size();i++){
			int length=line.get(i).size();
			BitString t=new BitString(length+1);
			for(int j=0;j<position;j++){
				t.set(j, line.get(i).get(j));
			}
			t.set(position,true);
			for(int j=position+1;j<length+1;j++){
				t.set(j, line.get(i).get(j-1));
			}
			temp.add(t);
		}
		res.add(temp);
		}
		return res;
	}
}
