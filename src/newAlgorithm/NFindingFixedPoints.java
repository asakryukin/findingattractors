package newAlgorithm;

import java.util.ArrayList;
import java.util.List;

import mainPackage.BitString;
import mainPackage.FVSComparator;

public class NFindingFixedPoints extends NewAlgBoolean{
	private List<String> FP;
	private boolean stop=false;
	public NFindingFixedPoints(String filename) {
		super(filename);
		// TODO Auto-generated constructor stub
		FP=findStableStates();
	}
	
	public List<String>  getFixedPoints() {
		// TODO Auto-generated method stub
		return FP;
	}
	
	@Override
	protected List<List<String>> findAttractor() {
		// TODO Auto-generated method stub
		List<List<String>> result=new ArrayList<List<String>>();
		return result;
	}
	
	@Override
	protected List<String> findStableStates() {
		// TODO Auto-generated method stub

		List<BitString> result=new ArrayList<BitString>();
		List<Integer> nodesIn=new ArrayList<Integer>();
		List<Integer> nodesNotIn=new ArrayList<Integer>();
		for(int i=0;i<numberOfNodes;i++){
			nodesNotIn.add(i);
		}
		for(int i=0;i<numberOfNodes;i++){
			int best=0;
			int node_number=-1;
			List<BitString> tentative_result=new ArrayList<BitString>(); 
			//List<List<BitString>> candidateResults=new ArrayList<List<BitString>>(); 
			//List<Integer> candidates=new ArrayList<Integer>();
			System.out.print("Adding node number:"+(i+1));
			for (int k=0;k<nodesNotIn.size();k++){
				//start checking different combinations
				int attr_length=0;
				nodesIn.add(nodesNotIn.get(k));
				nodesIn.sort(new FVSComparator());
				//System.out.println();
				//System.out.print("Adding node number:"+(i+1));
				List<BitString> temp=new ArrayList<BitString>();
				
				List<List<BitString>> t=updateEntry(result, nodesIn.indexOf(nodesNotIn.get(k)));
					temp.addAll(t.get(0));
					temp.addAll(t.get(1));
						//counting DFS size, by eliminating
						for(int j=0;j<temp.size();j++){
							//int curr=usedNodes;
							if(!ifN(temp.get(j), nodesIn)){
								temp.remove(j);
								j--;
								
								
							}
						}
						attr_length=temp.size();
						if(stop){
							//if already longer remove it
							nodesIn.remove(nodesIn.indexOf(nodesNotIn.get(k)));
							stop=false;
							continue;
						}
						if(best==0||best>attr_length){
							//if it is better
							//candidates=new ArrayList<Integer>();
							//candidateResults=new ArrayList<List<BitString>>();
							best=attr_length;
							node_number=nodesNotIn.get(k);
							
							tentative_result=new ArrayList<BitString>();
							for(int ind=0;ind<temp.size();ind++){
								tentative_result.add(temp.get(ind));
							}
							/*
							candidates.add(node_number);
							candidateResults.add(tentative_result);
							*/
						}
						/*
						else if(best==attr_length){
							node_number=nodesNotIn.get(k);
							tentative_result=new ArrayList<BitString>();
							for(int ind=0;ind<temp.size();ind++){
								tentative_result.add(temp.get(ind));
							}
							candidates.add(node_number);
							candidateResults.add(tentative_result);
						}
						*/
						nodesIn.remove(nodesIn.indexOf(nodesNotIn.get(k)));
						
						//System.out.println(" Number of Representative States:"+result.size());
			}
			//add best
			//int selected=(int) Math.floor(candidates.size()*Math.random());
			//nodesIn.add(candidates.get(selected));
			nodesIn.add(node_number);
			nodesIn.sort(new FVSComparator());
			//nodesNotIn.remove(nodesNotIn.indexOf(candidates.get(selected)));
			nodesNotIn.remove(nodesNotIn.indexOf(node_number));
			result=new ArrayList<BitString>();
			/*
			for(int ind=0;ind<candidateResults.get(selected).size();ind++){
				result.add(candidateResults.get(selected).get(ind));
			}
			*/
			for(int ind=0;ind<tentative_result.size();ind++){
				result.add(tentative_result.get(ind));
			}
			System.out.println(" Number of Representative States:"+result.size());
			System.out.println();
		
		}
		/*
		nodesIn.add(nodesNotIn.get(0));
		nodesIn.sort(new FVSComparator());
		
		System.out.println();
		System.out.print("Adding node number:"+(nodesNotIn.get(0)+1));
		
		List<BitString> temp=new ArrayList<BitString>();
	
		List<List<BitString>> t=updateEntry(result, nodesIn.indexOf(nodesNotIn.get(0)));
		temp.addAll(t.get(0));
		temp.addAll(t.get(1));
		result=temp;
		*/
		List<String> stringResult=new ArrayList<String>();
		
		for(int i=0;i<result.size();i++){
			String tt="";
			for(int j=0;j<result.get(i).size();j++){
				if(result.get(i).get(j)){
					tt+="1";
				}else{
					tt+="0";
				}
			}
			stringResult.add(tt);
		}
		
		return stringResult;
	}
	
	
	protected boolean ifN(BitString candidate,List<Integer> nodesIn){
		//IMPORTANT: nodes are added in order
		
		List<BitString> result=new ArrayList<BitString>();
		for(int ind=0;ind<candidate.size();ind++){
			List<Integer> neighbours=listOfNeighbours.get(nodesIn.get(ind));
			List<Integer> notIn=new ArrayList<Integer>();
			notIn=whetherAllIn(nodesIn, neighbours);
			if(notIn.isEmpty()){
				
					
					//BitString var=new BitString(candidate.size());
					//var.or(candidate);
					int sum=0;
					boolean newVal=false;
					for(int j=0;j<neighbours.size();j++){
						if(candidate.get(nodesIn.indexOf(neighbours.get(j)-1)))
						sum+=Math.pow(2, neighbours.size()-1-j);
					}
					if(truthTable.get(nodesIn.get(ind)).get(sum))
						newVal=true;
					if(candidate.get(nodesIn.indexOf(nodesIn.get(ind)))!=newVal){
						return false;
						/*var.flip(nodesIn.indexOf(nodesIn.get(ind)));
						
						if(!containsBitString(result, var)){
							result.add(var);
							return false;
						}*/
					}
				
			}else{
				//notIn.sort(new SimpleIntComparator());
				//BitString var=new BitString(candidate.size());
				//var.or(candidate);
					boolean changing=true;
					int[] nVal=new int[neighbours.size()];
					
					for(int j=0;j<Math.pow(2, notIn.size());j++){
						int[] temp=new int[notIn.size()];
						int val=j;
						for(int k=0;k<notIn.size();k++){
							temp[notIn.size()-k-1]=val%2;
							val/=2;
						}
						
					for(int k=0;k<neighbours.size();k++){
						if(notIn.contains(neighbours.get(k))){
							nVal[k]=temp[notIn.indexOf(neighbours.get(k))];
						}else{
							if(candidate.get(nodesIn.indexOf(neighbours.get(k)-1))){
								nVal[k]=1;
							}else{
								nVal[k]=0;
							}
						}
					}
					int sum=0;
					boolean newVal=false;
					for(int k=0;k<neighbours.size();k++){
						sum+=nVal[k]*Math.pow(2, neighbours.size()-1-k);
					}
					if(truthTable.get(nodesIn.get(ind)).get(sum))
						newVal=true;
					if(candidate.get(nodesIn.indexOf(nodesIn.get(ind)))==newVal){
						changing=false;
						break;
						}
					}
					if(changing){
						return false;
						/*
						boolean newVal=false;
						if(newVal==var.get(nodesIn.indexOf(nodesIn.get(ind))))
							newVal=true;
						var.set(nodesIn.indexOf(nodesIn.get(ind)),newVal);
						
						if(!containsBitString(result, var)){
							result.add(var);
							return false;
						}*/
					}
					
				}
		}
		return true;
	}
	
	
}
