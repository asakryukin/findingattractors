package launcher;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import mainPackage.Graph;
import mainPackage.SinkTest;
import newAlgorithm.NewAlg;
import cellularAutomata.CreateCellularAutomataDataset;

public class AutomataMaxTest {
	private int rule=126;
	private String filename="cell.txt"; 
	private PrintWriter writer;
	 public AutomataMaxTest(int r) {
		// TODO Auto-generated constructor stub
		 rule=r;
		 try {
			writer=new PrintWriter("report_"+rule+".txt", "UTF-8");
			writer.println("Rule number:"+rule);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		runTesting(); 
	}
	 
	 private void runTesting(){
		 //writer.println("Simple algorithm:");
		 //runFirstTest();
		 //writer.println("From the paper:");
		 //runSecondTest();
		 //writer.println("New algorithm:");
		 runThirdTest();
		 writer.close();
	 }
	 
	 private void runFirstTest(){
		 int numberOfNodes=4;
		 while(true){
			 try{
				 CreateCellularAutomataDataset CCAD=new CreateCellularAutomataDataset(numberOfNodes, rule, filename);
				 CCAD.generateFile();
				 SinkTest g=new SinkTest(filename);
				 writer.print("Number of nodes:"+numberOfNodes+"\t Number of Attractors:"+g.getAttractors().size()+"\t Number of Elements in attractor:\t");
				 for(int i=0;i<g.getAttractors().size();i++){
					 writer.print(""+g.getAttractors().get(i).size()+"\t");
				 }
				 writer.print("Number of visited nodes: "+g.numberOfVisitedNodes());
				 writer.println();
			 }catch(StackOverflowError e){
				 System.out.println("NumberOfNodes:"+numberOfNodes+" "+e.toString());
				 writer.println("Number of Nodes:"+numberOfNodes+"\t Error:"+e.toString());
				 writer.println();
				 writer.println();
				 break;
			 }catch(OutOfMemoryError e){
				 System.out.println("NumberOfNodes:"+numberOfNodes+" "+e.toString());
				 writer.println("Number of Nodes:"+numberOfNodes+"\t Error:"+e.toString());
				 writer.println();
				 writer.println();
				 break;
			 }
			 numberOfNodes++;
			 System.out.println(numberOfNodes);
		 }
	 }
	 
	 private void runSecondTest(){
		 int numberOfNodes=4;
		 while(true){
			 try{
				 CreateCellularAutomataDataset CCAD=new CreateCellularAutomataDataset(numberOfNodes, rule, filename);
				 CCAD.generateFile();
				 Graph g=new Graph(filename);
				 writer.print("Number of nodes:"+numberOfNodes+"\t Number of Attractors:"+g.attractors().size()+"\t Number of Elements in attractor:\t");
				 for(int i=0;i<g.attractors().size();i++){
					 writer.print(""+g.attractors().get(i).size()+"\t");
				 }
				 writer.print("Number of visited nodes: "+g.numberOfVisitedNodes());
				 writer.println();
			 }catch(StackOverflowError e){
				 System.out.println("NumberOfNodes:"+numberOfNodes+" "+e.toString());
				 writer.println("Number of Nodes:"+numberOfNodes+"\t Error:"+e.toString());
				 writer.println();
				 writer.println();
				 break;
			 }catch(OutOfMemoryError e){
				 System.out.println("NumberOfNodes:"+numberOfNodes+" "+e.toString());
				 writer.println("Number of Nodes:"+numberOfNodes+"\t Error:"+e.toString());
				 writer.println();
				 writer.println();
				 break;
			 }
			 numberOfNodes++;
		 }
	 }
	 
	 private void runThirdTest(){
		 int numberOfNodes=4;
		 while(true){
			 try{
				 CreateCellularAutomataDataset CCAD=new CreateCellularAutomataDataset(numberOfNodes, rule, filename);
				 CCAD.generateFile();
				 NewAlg g=new NewAlg(filename);
				 writer.print("Number of nodes:"+numberOfNodes+"\t Number of Attractors:"+g.getAttractors().size()+"\t Number of Elements in attractors:\t");
				 //int s=0;
				 for(int i=0;i<g.getAttractors().size();i++){
					 //s+=g.getAttractors().get(i).size();
					 writer.print(""+g.getAttractors().get(i).size()+"\t");
				 }
				 //writer.print(""+s+"\t");
				 writer.print("Number of visited nodes: "+g.numberOfVisitedNodes());
				 writer.println();
			 }catch(StackOverflowError e){
				 System.out.println("NumberOfNodes:"+numberOfNodes+" "+e.toString());
				 writer.println("Number of Nodes:"+numberOfNodes+"\t Error:"+e.toString());
				 writer.println();
				 writer.println();
				 break;
			 }catch(OutOfMemoryError e){
				 System.out.println("NumberOfNodes:"+numberOfNodes+" "+e.toString());
				 writer.println("Number of Nodes:"+numberOfNodes+"\t Error:"+e.toString());
				 writer.println();
				 writer.println();
				 break;
			 }
			 writer.flush();
			 numberOfNodes++;
		 }
	 }
}
