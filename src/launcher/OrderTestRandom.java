package launcher;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import newAlgorithm.NewAlg;
import newAlgorithm.NewGreedyAlgorithm;
import datasetcreator.CreateTestingSet;

public class OrderTestRandom {
	private PrintWriter writer;
	private String filename="data.txt";
	public OrderTestRandom() {
		// TODO Auto-generated constructor stub
		try {
			
			for(int numberOfNodes=10;numberOfNodes<11;numberOfNodes++){
				//for(int numberOfTests=0;numberOfTests<10;numberOfTests++)
					writer=new PrintWriter("report_testing_"+numberOfNodes+".txt", "UTF-8");
					CreateTestingSet CT=new CreateTestingSet(filename, numberOfNodes, 3);
					CT.generateFile();
					try{
					NewAlg g=new NewAlg(filename);
					writer.print("Number of nodes:"+numberOfNodes+"\t Number of Attractors:"+g.getAttractors().size()+"\t Number of Elements in attractor:\t");
					for(int i=0;i<g.getAttractors().size();i++){
						 //s+=g.getAttractors().get(i).size();
						 writer.print(""+g.getAttractors().get(i).size()+"\t");
					 }
					writer.println();
					writer.print("Number of visited nodes (ORDERED): "+g.numberOfVisitedNodes()+"\t");
					for (int i=0;i<99;i++){
						g=new NewAlg(filename);
						writer.print(""+g.numberOfVisitedNodes()+"\t");
						writer.flush();
					}
					writer.println();
					//g=new NewAlg(filename,false);
					//writer.print("Number of visited nodes (PRIORITY ORDERING): "+g.numberOfVisitedNodes()+"\t");
					}catch(StackOverflowError e){
						 System.out.println("NumberOfNodes:"+numberOfNodes+" "+e.toString());
						 writer.println("Number of Nodes:"+numberOfNodes+"\t Error:"+e.toString());
						 writer.println();
						 writer.println();
						 break;
					 }catch(OutOfMemoryError e){
						 System.out.println("NumberOfNodes:"+numberOfNodes+" "+e.toString());
						 writer.println("Number of Nodes:"+numberOfNodes+"\t Error:"+e.toString());
						 writer.println();
						 writer.println();
						 break;
					 }
					try{
						
						NewAlg g=new NewAlg(filename,false);
						writer.print("Number of visited nodes (PRIORITY ORDERING): "+g.numberOfVisitedNodes()+"\t");
						for (int i=0;i<99;i++){
							g=new NewAlg(filename,false);
							writer.print(""+g.numberOfVisitedNodes()+"\t");
							writer.flush();
						}
						writer.println();
						}catch(StackOverflowError e){
							 System.out.println(" "+e.toString());
							 writer.println("\t Error:"+e.toString());
							 writer.println();
							 writer.println();
							 break;
						 }catch(OutOfMemoryError e){
							 System.out.println(" "+e.toString());
							 writer.println("\t Error:"+e.toString());
							 writer.println();
							 writer.println();
							 break;
						 }
					try{
						
						NewGreedyAlgorithm g=new NewGreedyAlgorithm(filename);
						writer.print("Number of visited nodes (GREEDY): "+g.numberOfVisitedNodes()+"\t");
						for (int i=0;i<99;i++){
							g=new NewGreedyAlgorithm(filename);
							writer.print(""+g.numberOfVisitedNodes()+"\t");
							writer.flush();
						}
						writer.println();
						}catch(StackOverflowError e){
							 System.out.println(" "+e.toString());
							 writer.println("\t Error:"+e.toString());
							 writer.println();
							 writer.println();
							 break;
						 }catch(OutOfMemoryError e){
							 System.out.println(" "+e.toString());
							 writer.println("\t Error:"+e.toString());
							 writer.println();
							 writer.println();
							 break;
						 }
					writer.println();
					writer.flush();
				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
