package launcher;

import java.util.List;

import mainPackage.Graph;
import mainPackage.SinkTest;
import newAlgorithm.FindAttractorsByMerging;
import newAlgorithm.FindFixedPointsByMerging;
import newAlgorithm.FindingFixedPointsPriority;
import newAlgorithm.NFindingFixedPoints;
import newAlgorithm.NewAlgBoolean;
import newAlgorithm.NewGreedyAlgorithmBoolean;
import cellularAutomata.CreateCellularAutomataDataset;
import datasetcreator.CreateTestingSet;

public class RandomTest {
	private static List<List<String>> attractors,attractorsSink,attractorNew;
	private static int trials=0,successfulTrials=0;
	private String filename="dataset.txt";
	public RandomTest() {
		// TODO Auto-generated constructor stub
		// TODO Auto-generated method stub
				CreateCellularAutomataDataset CCAD=new CreateCellularAutomataDataset(9, 254, "cell.txt");
				CCAD.generateFile();
				for(int numberOfNodes=3;numberOfNodes<10;numberOfNodes++){
					for(int numberOfTests=0;numberOfTests<10;numberOfTests++){
						trials++;
						CreateTestingSet CT=new CreateTestingSet(filename, numberOfNodes, 2);
						CT.generateFile();
						
						//SinkTest sT=new SinkTest(filename);
						FindAttractorsByMerging sT=new FindAttractorsByMerging(filename);
						Graph graph=new Graph(filename);
						NewAlgBoolean nA=new NewAlgBoolean(filename);
						//NewGreedyAlgorithmBoolean nA=new NewGreedyAlgorithmBoolean(filename);
					
						attractors=graph.attractors();
						attractorsSink=sT.getAttractors();
						attractorNew=nA.getAttractors();
						printOutAttractors(attractors);
						printOutAttractors(attractorsSink);
						printOutAttractors(attractorNew);
					
						if(attractors.equals(attractorNew)&&attractors.equals(attractorsSink)){
							successfulTrials++;
							System.out.println("EQUAL!");
						}else{
							System.out.println("-------------------- NOT EQUAL! ------------------");
						}
					
						//NFindingFixedPoints NF=new NFindingFixedPoints(filename);
						//FindingFixedPointsPriority NFP=new FindingFixedPointsPriority(filename);
						//FindFixedPointsByMerging FB=new FindFixedPointsByMerging(filename);
						/*
						if(NF.getFixedPoints().equals(FB.getFixedPoints())){
							successfulTrials++;
							System.out.println("EQUAL!");
						}else{
							System.out.println("-------------------- NOT EQUAL! ------------------");
						}
						*/
					}
				}
				System.out.println("Number of tests:"+trials);
				System.out.println("Successful:"+successfulTrials);
	}
	static private void printOutAttractors(List<List<String>> attractors){
		for(int i=0;i<attractors.size();i++){
			System.out.println("Attractor #"+(i+1));
			
			for(int j=0;j<attractors.get(i).size();j++){
				System.out.print(""+attractors.get(i).get(j)+"\t");
			}
			System.out.println();
		}
		System.out.println();
	}
}
