package mainPackage;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;

import newAlgorithm.FindFixedPointsByMerging;


public class main {
	
	private static PrintWriter writer;
	public static void main(String[] args) {
		//RandomTest RT=new RandomTest();
		//RandomTest RT=new RandomTest();
		
		System.out.println("NNER");
		String filename="";
		Scanner keyboard = new Scanner(System.in);
		System.out.println("enter file name");
		filename=keyboard.nextLine();
		//FindAttractorsByMerging FB=new FindAttractorsByMerging(filename);
		
		FindFixedPointsByMerging FB=new FindFixedPointsByMerging(filename);
		
		System.out.println("Number of fixed points:"+FB.getFixedPoints().size());
		//System.out.println("Number of elements:"+attractorlength(FB.getAttractors()));
		
		//CreateTestingSet CTS=new CreateTestingSet("data.txt", 24, 3);
		//CTS.generateFile();
		/*
		System.out.println("NNER");
		String filename="";
		Scanner keyboard = new Scanner(System.in);
		System.out.println("enter file name");
		filename=keyboard.nextLine();
		//NewGreedyAlgorithmBoolean NAB = new NewGreedyAlgorithmBoolean(filename);
		//List<String> F=new ArrayList<String>();
		long startTime = System.currentTimeMillis();
		//FindingFixedPointsPriority NF=new FindingFixedPointsPriority(filename);
		NFindingFixedPoints NF=new NFindingFixedPoints(filename);
		System.out.println(""+NF.getFixedPoints().size());
		long stopTime = System.currentTimeMillis();
		System.out.println("Execution time: "+(stopTime-startTime)+"ms");
		
		/*
		System.out.println("Greedy");
		String filename="";
		Scanner keyboard = new Scanner(System.in);
		System.out.println("enter file name");
		filename=keyboard.nextLine();
		NewGreedyAlgorithmBoolean NAB;
		try {
			writer=new PrintWriter("report_greedy"+".txt", "UTF-8");
			long startTime = System.currentTimeMillis();
			NAB=new NewGreedyAlgorithmBoolean(filename);
			//System.out.println("Execution time: "+(stopTime-startTime)+"ms");
			writer.println("Elements:"+attractorlength(NAB.getAttractors()));
			writer.println(""+NAB.numberOfVisitedNodes());
			writer.flush();
			
			for(int i=0;i<99;i++){
				NAB=new NewGreedyAlgorithmBoolean(filename);
				writer.println(""+NAB.numberOfVisitedNodes());
				writer.flush();
			}
			long stopTime = System.currentTimeMillis();
			System.out.println("Execution time: "+(stopTime-startTime)+"ms");
			writer.println("Attractor length:"+attractorlength(NAB.getAttractors()));
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		System.out.println("Indexed");
		String filename="";
		Scanner keyboard = new Scanner(System.in);
		System.out.println("enter file name");
		filename=keyboard.nextLine();
		NewAlgBoolean NAB;
		try {
			writer=new PrintWriter("report_indexed"+".txt", "UTF-8");
			long startTime = System.currentTimeMillis();
			NAB=new NewAlgBoolean(filename);
			writer.println("Elements:"+attractorlength(NAB.getAttractors()));
			writer.println(""+NAB.numberOfVisitedNodes());
			writer.flush();
			for(int i=0;i<99;i++){
				NAB=new NewAlgBoolean(filename);
				writer.println(""+NAB.numberOfVisitedNodes());
				writer.flush();
			}
			long stopTime = System.currentTimeMillis();
			System.out.println("Execution time: "+(stopTime-startTime)+"ms");
			writer.println("Attractor length:"+attractorlength(NAB.getAttractors()));
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		System.out.println("Priority");
		String filename="";
		Scanner keyboard = new Scanner(System.in);
		System.out.println("enter file name");
		filename=keyboard.nextLine();
		NewAlgBoolean NAB;
		try {
			writer=new PrintWriter("report_priority"+".txt", "UTF-8");
			long startTime = System.currentTimeMillis();
			NAB=new NewAlgBoolean(filename,false);
			writer.println("Elements:"+attractorlength(NAB.getAttractors()));
			writer.println(""+NAB.numberOfVisitedNodes());
			writer.flush();
			for(int i=0;i<99;i++){
				NAB=new NewAlgBoolean(filename,false);
				writer.println(""+NAB.numberOfVisitedNodes());
				writer.flush();
			}
			long stopTime = System.currentTimeMillis();
			System.out.println("Execution time: "+(stopTime-startTime)+"ms");
			writer.println("Attractor length:"+attractorlength(NAB.getAttractors()));
			writer.close();
		} catch (FileNotFoundException | UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*
		long startTime = System.currentTimeMillis();
		//NewAlg NA=new NewAlg("data.txt");
		long stopTime = System.currentTimeMillis();
		System.out.println("Execution time: "+(stopTime-startTime)+"ms");
		startTime = System.currentTimeMillis();
		NewGreedyAlgorithm NAB=new NewGreedyAlgorithm("data.txt");
		stopTime = System.currentTimeMillis();
		System.out.println("Execution time: "+(stopTime-startTime)+"ms");
		//printOutAttractors(NAB.getAttractors());
		
		/*
		String line="some line";
		int position = 5;
		long startTime = System.currentTimeMillis();
		for(int i=0;i<1000000;i++){
			String t=line.substring(0, position);
			t+="0";
			t+=line.substring(position);
			
		}
		long stopTime = System.currentTimeMillis();
		System.out.println("Execution time: "+(stopTime-startTime)+"ms");
		StringBuffer lB=new StringBuffer("some line");
		BitString l=new BitString(9);

		startTime = System.currentTimeMillis();
		int length=line.length();
		for(int i=0;i<1000000;i++){
			
			BitString t=new BitString(length+1);
			for(int j=0;j<position;j++){
				t.set(j, l.get(j));
			}
			t.set(position,false);
			for(int j=position+1;j<length+1;j++){
				t.set(j, l.get(j-1));
			}
			
		}
		stopTime = System.currentTimeMillis();
		System.out.println("Execution time: "+(stopTime-startTime)+"ms");
		*/
		/*
		long startTime = System.currentTimeMillis();
		OrderTestRandom O=new OrderTestRandom();
		long stopTime = System.currentTimeMillis();
		System.out.println("Execution time: "+(stopTime-startTime)+"ms");
		//NewGreedyAlgorithm NG=new NewGreedyAlgorithm("data.txt");
		
		//AutomataMaxTest AMT=new AutomataMaxTest(126);
		
		/*
		long startTime = System.currentTimeMillis();
		CreateCellularAutomataDataset CCAD=new CreateCellularAutomataDataset(15, 254, "data.txt");
		
		CCAD.generateFile();
		/*
		String filename="";
		Scanner keyboard = new Scanner(System.in);
		System.out.println("enter file name");
		filename=keyboard.nextLine();
		*/
		/*
		NewAlg NA=new NewAlg("data.txt");
		long stopTime = System.currentTimeMillis();
		printOutAttractors(NA.getAttractors());
		System.out.println("Execution time: "+(stopTime-startTime)+"ms");
		/*
		int n=0;
		
		while(n!=-1){
			System.out.println("Type the number of the attractor to print out or -1 to exit:");
			n=keyboard.nextInt();
			System.out.println();
			if(n>-1){
				if(n<=NA.getAttractors().size()&&n!=0){
				for(int j=0;j<NA.getAttractors().get(n-1).size();j++){
					System.out.print(""+NA.getAttractors().get(n-1).get(j)+"\t");
				}
				}else{
					System.out.println("There is no such attractor.");
				}
			}
			System.out.println();
		}
		
	*/
		//AutomataMaxTest AMT3=new AutomataMaxTest(126);
		//AutomataMaxTest AMT4=new AutomataMaxTest(100);
		//AutomataMaxTest AMT5=new AutomataMaxTest(33);
		
		//NewAlg NA=new NewAlg("data.txt");
		//printOutAttractors(NA.getAttractors());
		
		//SinkTest ST=new SinkTest("cell.txt");
		//System.out.println(ST.numberOfVisitedNodes());
		/*Graph g=new Graph("problem2.txt");
		System.out.println(g.numberOfVisitedNodes());
		for(int i=0;i<g.attractors().size();i++){
			System.out.println();
			for(int j=0;j<g.attractors().get(i).size();j++){
				System.out.print(g.attractors().get(i).get(j)+"\t");
			}
			System.out.println(g.attractors().get(i).size());
		}
		
		NewAlg NA=new NewAlg("problem2.txt");
		System.out.println(NA.numberOfVisitedNodes());
		*/
	}

	
	static private int attractorlength(List<List<String>> attractors){
		int res=0;
		for(int i=0;i<attractors.size();i++){
			res+=attractors.get(i).size();
		}
		return res;
	}
	static private void printOutAttractors(List<List<String>> attractors){
		for(int i=0;i<attractors.size();i++){
			System.out.print("Attractor #"+(i+1));
			System.out.println(" size: "+attractors.get(i).size()+" elements ");
			for(int j=0;j<attractors.get(i).size();j++){
				System.out.print(""+attractors.get(i).get(j)+"\t");
			}
			System.out.println();
		}
		System.out.println();
	}
	
}
