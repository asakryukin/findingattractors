package mainPackage;

import java.util.ArrayList;
import java.util.List;

public class BitString{
	
	public  boolean[] value;
	public int length;
	
	public BitString(int length) {
		// TODO Auto-generated constructor stub
		value=new boolean[length];
		this.length=length;
		//for(int i=0;i<length;i++){
		//	value.add(false);
		//}
	}
	
	
	public boolean equalsBS(BitString c){
		if(c.size()!=length)
			return false;
		for(int i=0;i<c.size();i++){
			if(c.get(i)!=value[i])
				return false;
		}
		return true;
	}
	
	public Boolean get(int i){
		return value[i];
	}
	
	public int size(){
		return length;
	}
	
	public void or(BitString x){
		for(int i=0;i<x.size();i++){
			if(x.get(i)){
				value[i]=true;
			}
		}
	}
	
	
	public void flip(int i){
		
			value[i]=!value[i];
		
	}
	
	public void set(int i,boolean val){
		
		value[i]= val;
	}
	
	
}
