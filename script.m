figure(1)
%subplot(2,3,1)
%title('rule 100')

plot(1:11,log(elements100))
xlabel('number of nodes')
ylabel('elements in attractor')
hold on
%subplot(2,3,2)
title('rule 126')
plot(1:14,log(elements126))
xlabel('number of nodes')
ylabel('elements in attractor')

%subplot(2,3,3)
title('rule 129')
plot(1:14,log(elements129))
xlabel('number of nodes')
ylabel('elements in attractor')

%subplot(2,3,4)
title('rule 230')
plot(1:9,log(elements230))
xlabel('number of nodes')
ylabel('elements in attractor')

%subplot(2,3,5)
title('rule 254')
plot(1:14,log(elements254))
xlabel('number of nodes')
ylabel('elements in attractor')

figure(2)
subplot(2,3,1)
title('rule 100')
plot(1:11,stepsP100)
hold on
plot(1:11,stepsN100)
xlabel('number of nodes')
ylabel('number of steps')

subplot(2,3,2)
title('rule 126')
plot(1:14,stepsP126)
hold on
plot(1:14,stepsN126)
xlabel('number of nodes')
ylabel('number of steps')

subplot(2,3,3)
title('rule 129')
plot(1:14,stepsP129)
hold on
plot(1:14,stepsN129)
xlabel('number of nodes')
ylabel('number of steps')

subplot(2,3,4)
title('rule 230')
plot(1:9,stepsP230)
hold on
plot(1:9,stepsN230)
xlabel('number of nodes')
ylabel('number of steps')

subplot(2,3,5)
title('rule 254')
plot(1:14,stepsP254)
hold on
plot(1:14,stepsN254)
xlabel('number of nodes')
ylabel('number of steps')