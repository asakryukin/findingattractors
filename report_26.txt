Rule number:26
Simple algorithm:
Number of nodes:4	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 16
Number of nodes:5	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 32
Number of nodes:6	 Number of Attractors:4	 Number of Elements in attractor:	1	1	1	1	Number of visited nodes: 64
Number of nodes:7	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 128
Number of nodes:8	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 256
Number of nodes:9	 Number of Attractors:4	 Number of Elements in attractor:	1	1	1	1	Number of visited nodes: 512
Number of nodes:10	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 1024
Number of nodes:11	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 2048
Number of nodes:12	 Number of Attractors:4	 Number of Elements in attractor:	1	1	1	1	Number of visited nodes: 4096
Number of Nodes:13	 Error:java.lang.OutOfMemoryError: Java heap space


From the paper:
Number of nodes:4	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 33
Number of nodes:5	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 54
Number of nodes:6	 Number of Attractors:4	 Number of Elements in attractor:	1	1	1	1	Number of visited nodes: 100
Number of nodes:7	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 263
Number of nodes:8	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 498
Number of nodes:9	 Number of Attractors:4	 Number of Elements in attractor:	1	1	1	1	Number of visited nodes: 875
Number of nodes:10	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 1999
Number of nodes:11	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 3851
Number of nodes:12	 Number of Attractors:4	 Number of Elements in attractor:	1	1	1	1	Number of visited nodes: 8181
Number of nodes:13	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 16335
Number of nodes:14	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 38140
Number of nodes:15	 Number of Attractors:4	 Number of Elements in attractor:	1	1	1	1	Number of visited nodes: 71820
Number of nodes:16	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 172851
Number of nodes:17	 Number of Attractors:1	 Number of Elements in attractor:	1	Number of visited nodes: 335600
Number of nodes:18	 Number of Attractors:4	 Number of Elements in attractor:	1	1	1	1	Number of visited nodes: 801006
Number of Nodes:19	 Error:java.lang.StackOverflowError


New algorithm:
Number of nodes:4	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 39
Number of nodes:5	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 79
Number of nodes:6	 Number of Attractors:4	 Number of Elements in attractors:	1	1	1	1	Number of visited nodes: 140
Number of nodes:7	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 262
Number of nodes:8	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 471
Number of nodes:9	 Number of Attractors:4	 Number of Elements in attractors:	1	1	1	1	Number of visited nodes: 761
Number of nodes:10	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 1310
Number of nodes:11	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 2275
Number of nodes:12	 Number of Attractors:4	 Number of Elements in attractors:	1	1	1	1	Number of visited nodes: 3722
Number of nodes:13	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 7068
Number of nodes:14	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 10313
Number of nodes:15	 Number of Attractors:4	 Number of Elements in attractors:	1	1	1	1	Number of visited nodes: 20228
Number of nodes:16	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 31027
Number of nodes:17	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 59777
Number of nodes:18	 Number of Attractors:4	 Number of Elements in attractors:	1	1	1	1	Number of visited nodes: 84441
Number of nodes:19	 Number of Attractors:1	 Number of Elements in attractors:	1	Number of visited nodes: 179153
Number of Nodes:20	 Error:java.lang.StackOverflowError


