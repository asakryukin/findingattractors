Rule number:230
Simple algorithm:
Number of nodes:4	 Number of Attractors:2	 Number of Elements in attractor:	1	12	Number of visited nodes: 16
Number of nodes:5	 Number of Attractors:1	 Number of Elements in attractor:	24	Number of visited nodes: 32
Number of nodes:6	 Number of Attractors:1	 Number of Elements in attractor:	48	Number of visited nodes: 64
Number of nodes:7	 Number of Attractors:1	 Number of Elements in attractor:	96	Number of visited nodes: 128
Number of nodes:8	 Number of Attractors:1	 Number of Elements in attractor:	192	Number of visited nodes: 256
Number of nodes:9	 Number of Attractors:1	 Number of Elements in attractor:	384	Number of visited nodes: 512
Number of nodes:10	 Number of Attractors:1	 Number of Elements in attractor:	768	Number of visited nodes: 1024
Number of nodes:11	 Number of Attractors:1	 Number of Elements in attractor:	1536	Number of visited nodes: 2048
Number of nodes:12	 Number of Attractors:1	 Number of Elements in attractor:	3072	Number of visited nodes: 4096
Number of nodes:13	 Number of Attractors:1	 Number of Elements in attractor:	6144	Number of visited nodes: 8192
Number of Nodes:14	 Error:java.lang.OutOfMemoryError: Java heap space


From the paper:
Number of nodes:4	 Number of Attractors:2	 Number of Elements in attractor:	1	12	Number of visited nodes: 32
Number of nodes:5	 Number of Attractors:1	 Number of Elements in attractor:	24	Number of visited nodes: 67
Number of nodes:6	 Number of Attractors:1	 Number of Elements in attractor:	48	Number of visited nodes: 132
Number of nodes:7	 Number of Attractors:1	 Number of Elements in attractor:	96	Number of visited nodes: 249
Number of nodes:8	 Number of Attractors:1	 Number of Elements in attractor:	192	Number of visited nodes: 493
Number of nodes:9	 Number of Attractors:1	 Number of Elements in attractor:	384	Number of visited nodes: 981
Number of nodes:10	 Number of Attractors:1	 Number of Elements in attractor:	768	Number of visited nodes: 1954
Number of nodes:11	 Number of Attractors:1	 Number of Elements in attractor:	1536	Number of visited nodes: 3851
Number of nodes:12	 Number of Attractors:1	 Number of Elements in attractor:	3072	Number of visited nodes: 7653
Number of Nodes:13	 Error:java.lang.StackOverflowError


New algorithm:
Number of nodes:4	 Number of Attractors:2	 Number of Elements in attractors:	1	12	Number of visited nodes: 62
Number of nodes:5	 Number of Attractors:1	 Number of Elements in attractors:	24	Number of visited nodes: 184
Number of nodes:6	 Number of Attractors:1	 Number of Elements in attractors:	48	Number of visited nodes: 244
Number of nodes:7	 Number of Attractors:1	 Number of Elements in attractors:	96	Number of visited nodes: 556
Number of nodes:8	 Number of Attractors:1	 Number of Elements in attractors:	192	Number of visited nodes: 1152
Number of nodes:9	 Number of Attractors:1	 Number of Elements in attractors:	384	Number of visited nodes: 1894
Number of nodes:10	 Number of Attractors:1	 Number of Elements in attractors:	768	Number of visited nodes: 3796
Number of nodes:11	 Number of Attractors:1	 Number of Elements in attractors:	1536	Number of visited nodes: 10606
Number of nodes:12	 Number of Attractors:1	 Number of Elements in attractors:	3072	Number of visited nodes: 13460
Number of Nodes:13	 Error:java.lang.StackOverflowError


