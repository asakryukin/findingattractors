Rule number:129
Simple algorithm:
Number of nodes:4	 Number of Attractors:2	 Number of Elements in attractor:	1	7	Number of visited nodes: 16
Number of nodes:5	 Number of Attractors:2	 Number of Elements in attractor:	1	11	Number of visited nodes: 32
Number of nodes:6	 Number of Attractors:2	 Number of Elements in attractor:	1	18	Number of visited nodes: 64
Number of nodes:7	 Number of Attractors:2	 Number of Elements in attractor:	1	29	Number of visited nodes: 128
Number of nodes:8	 Number of Attractors:2	 Number of Elements in attractor:	1	47	Number of visited nodes: 256
Number of nodes:9	 Number of Attractors:2	 Number of Elements in attractor:	1	76	Number of visited nodes: 512
Number of Nodes:10	 Error:java.lang.OutOfMemoryError: Java heap space


From the paper:
Number of nodes:4	 Number of Attractors:2	 Number of Elements in attractor:	1	7	Number of visited nodes: 24
Number of nodes:5	 Number of Attractors:2	 Number of Elements in attractor:	1	11	Number of visited nodes: 44
Number of nodes:6	 Number of Attractors:2	 Number of Elements in attractor:	1	18	Number of visited nodes: 83
Number of nodes:7	 Number of Attractors:2	 Number of Elements in attractor:	1	29	Number of visited nodes: 158
Number of nodes:8	 Number of Attractors:2	 Number of Elements in attractor:	1	47	Number of visited nodes: 304
Number of nodes:9	 Number of Attractors:2	 Number of Elements in attractor:	1	76	Number of visited nodes: 589
Number of nodes:10	 Number of Attractors:2	 Number of Elements in attractor:	1	123	Number of visited nodes: 1148
Number of nodes:11	 Number of Attractors:2	 Number of Elements in attractor:	1	199	Number of visited nodes: 2248
Number of nodes:12	 Number of Attractors:2	 Number of Elements in attractor:	1	322	Number of visited nodes: 4419
Number of nodes:13	 Number of Attractors:2	 Number of Elements in attractor:	1	521	Number of visited nodes: 8714
Number of nodes:14	 Number of Attractors:2	 Number of Elements in attractor:	1	843	Number of visited nodes: 17228
Number of nodes:15	 Number of Attractors:2	 Number of Elements in attractor:	1	1364	Number of visited nodes: 34133
Number of nodes:16	 Number of Attractors:2	 Number of Elements in attractor:	1	2207	Number of visited nodes: 67744
Number of nodes:17	 Number of Attractors:2	 Number of Elements in attractor:	1	3571	Number of visited nodes: 134644
Number of Nodes:18	 Error:java.lang.StackOverflowError


New algorithm:
Number of nodes:4	 Number of Attractors:2	 Number of Elements in attractors:	1	7	Number of visited nodes: 60
Number of nodes:5	 Number of Attractors:2	 Number of Elements in attractors:	1	11	Number of visited nodes: 122
Number of nodes:6	 Number of Attractors:2	 Number of Elements in attractors:	1	18	Number of visited nodes: 192
Number of nodes:7	 Number of Attractors:2	 Number of Elements in attractors:	1	29	Number of visited nodes: 408
Number of nodes:8	 Number of Attractors:2	 Number of Elements in attractors:	1	47	Number of visited nodes: 564
Number of nodes:9	 Number of Attractors:2	 Number of Elements in attractors:	1	76	Number of visited nodes: 1030
Number of nodes:10	 Number of Attractors:2	 Number of Elements in attractors:	1	123	Number of visited nodes: 1456
Number of nodes:11	 Number of Attractors:2	 Number of Elements in attractors:	1	199	Number of visited nodes: 2284
Number of nodes:12	 Number of Attractors:2	 Number of Elements in attractors:	1	322	Number of visited nodes: 4266
Number of nodes:13	 Number of Attractors:2	 Number of Elements in attractors:	1	521	Number of visited nodes: 12186
Number of nodes:14	 Number of Attractors:2	 Number of Elements in attractors:	1	843	Number of visited nodes: 12082
Number of nodes:15	 Number of Attractors:2	 Number of Elements in attractors:	1	1364	Number of visited nodes: 32196
Number of nodes:16	 Number of Attractors:2	 Number of Elements in attractors:	1	2207	Number of visited nodes: 37298
Number of nodes:17	 Number of Attractors:2	 Number of Elements in attractors:	1	3571	Number of visited nodes: 27874
Number of Nodes:18	 Error:java.lang.StackOverflowError


